package com.tinkoff.sirius.koshelok;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class KoshelokApplication {

	public static void main(String[] args) {
		SpringApplication.run(KoshelokApplication.class, args);
	}

}
