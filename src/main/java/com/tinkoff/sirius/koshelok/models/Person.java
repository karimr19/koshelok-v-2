package com.tinkoff.sirius.koshelok.models;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.math.BigDecimal;

@Getter
@Setter
@Accessors(chain = true)
@Entity(name = "person")
@SequenceGenerator(allocationSize = 1, name = "person_seq", sequenceName = "person_seq")
public class Person {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "person_seq")
    private Long id;

    @Column(unique = true)
    private String email;

    @Column
    private BigDecimal balance;

    @Column
    private BigDecimal income;

    @Column
    private BigDecimal spendings;

}
