package com.tinkoff.sirius.koshelok.models;

import com.tinkoff.sirius.koshelok.types.CurrencyType;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@Accessors(chain = true)
@Entity(name = "Wallet")
@EntityListeners(AuditingEntityListener.class)
@SequenceGenerator(allocationSize = 1, name = "wallet_seq", sequenceName = "wallet_seq")
public class Wallet {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "wallet_seq")
    private Long id;

    @Column
    private Integer isHidden;

    @Column
    private String name;

    @Column
    @Enumerated(EnumType.STRING)
    private CurrencyType currency;

    @Column
    private BigDecimal amountLimit;

    @Column
    private BigDecimal balance;

    @Column
    private BigDecimal income;

    @Column
    private BigDecimal spendings;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "person_id")
    private Person person;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "wallet")
    private List<Operation> operations = new ArrayList<>();

}
