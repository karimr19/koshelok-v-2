package com.tinkoff.sirius.koshelok.services;

import com.tinkoff.sirius.koshelok.converters.CurrencyConverter;
import com.tinkoff.sirius.koshelok.converters.WalletConverter;
import com.tinkoff.sirius.koshelok.dto.CurrencyDto;
import com.tinkoff.sirius.koshelok.dto.WalletDto;
import com.tinkoff.sirius.koshelok.dto.WalletDtoResponse;
import com.tinkoff.sirius.koshelok.exceptions.AuthenticationException;
import com.tinkoff.sirius.koshelok.exceptions.NotFoundException;
import com.tinkoff.sirius.koshelok.models.Person;
import com.tinkoff.sirius.koshelok.models.Wallet;
import com.tinkoff.sirius.koshelok.repositories.CurrencyRepository;
import com.tinkoff.sirius.koshelok.repositories.PersonRepository;
import com.tinkoff.sirius.koshelok.repositories.WalletRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class WalletService {
    private final WalletConverter converter;

    private final CurrencyConverter currencyConverter;

    private final WalletRepository walletRepository;

    private final PersonRepository personRepository;

    private final OperationService operationService;

    private final CurrencyRepository currencyRepository;

    @Transactional
    public WalletDto createWallet(WalletDto walletDto, String email) {
        var person = personRepository.findByEmail(email).orElseThrow(
                () -> new AuthenticationException(email)
        );
        var wallet = converter.convert(walletDto);
        wallet.setPerson(personRepository.findById(person.getId())
                .orElseThrow(() -> new NotFoundException(Person.class, person.getId())));
        wallet.setIncome(new BigDecimal(0))
                .setBalance(new BigDecimal(0))
                .setSpendings(new BigDecimal(0));
        return converter.convert(walletRepository.save(wallet));
    }

    public List<WalletDtoResponse> getWalletsByPersonId(String email) {
        var person = personRepository.findByEmail(email).orElseThrow(
                () -> new AuthenticationException(email)
        );
        var wallets = walletRepository.findAllByPersonId(person.getId());
        List<WalletDtoResponse> walletDtoResponses = new ArrayList<>();
        for (Wallet wallet : wallets) {
            var currencyModel = currencyRepository.findByCode(wallet.getCurrency().toString());
            walletDtoResponses.add(converter.convertEx(wallet).setCurrencyDto(currencyConverter.convert(currencyModel)));
        }
        return walletDtoResponses;
    }

    public WalletDto getWalletByPersonId(Long walletId, String email) {
        var person = personRepository.findByEmail(email).orElseThrow(
                () -> new AuthenticationException(email)
        );
        return walletRepository.findByIdAndPersonId(walletId, person.getId())
                .map(converter::convert)
                .orElseThrow(() -> new NotFoundException(Wallet.class, walletId));
    }

    @Transactional
    public WalletDto updateWallet(Long walletId, String email, WalletDto walletDto) {
        var person = personRepository.findByEmail(email).orElseThrow(
                () -> new AuthenticationException(email)
        );
        var wallet = walletRepository.findByIdAndPersonId(walletId, person.getId())
                .orElseThrow(() -> new NotFoundException(Wallet.class, walletId));
        wallet.setAmountLimit(walletDto.getAmountLimit())
                .setIsHidden(walletDto.getIsHidden())
                .setName(walletDto.getName());
        var updatedWallet = walletRepository.save(wallet);
        return converter.convert(updatedWallet);
    }

    @Transactional
    public void deleteWallet(Long walletId, String email) {
        var person = personRepository.findByEmail(email).orElseThrow(
                () -> new AuthenticationException(email)
        );
        if (walletRepository.existsWalletByIdAndPersonId(walletId, person.getId())) {
            operationService.deleteAllWalletOperations(walletId, email);
            walletRepository.deleteByIdAndPersonId(walletId, person.getId());
        } else {
            throw new NotFoundException(Wallet.class, walletId);
        }
    }
}
