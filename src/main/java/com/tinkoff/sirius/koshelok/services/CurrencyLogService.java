package com.tinkoff.sirius.koshelok.services;

import com.tinkoff.sirius.koshelok.exceptions.NotFoundException;
import com.tinkoff.sirius.koshelok.models.CurrencyLog;
import com.tinkoff.sirius.koshelok.repositories.CurrencyLogRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import java.time.LocalDate;
@Service
@RequiredArgsConstructor
public class CurrencyLogService {
    private final CurrencyLogRepository currencyLogRepository;

    public CurrencyLog getCurrencyLogByDate(LocalDate date) {
        return currencyLogRepository.findByDate(
                date
        ).orElseThrow(
                () -> new NotFoundException(CurrencyLog.class, -1L)
        );
    }
}
