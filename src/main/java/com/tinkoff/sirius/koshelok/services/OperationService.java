package com.tinkoff.sirius.koshelok.services;

import com.tinkoff.sirius.koshelok.converters.OperationConverter;
import com.tinkoff.sirius.koshelok.dto.OperationRequestDto;
import com.tinkoff.sirius.koshelok.dto.OperationResponseDto;
import com.tinkoff.sirius.koshelok.exceptions.AuthenticationException;
import com.tinkoff.sirius.koshelok.exceptions.NotFoundException;
import com.tinkoff.sirius.koshelok.models.*;
import com.tinkoff.sirius.koshelok.repositories.*;
import com.tinkoff.sirius.koshelok.types.CurrencyType;
import com.tinkoff.sirius.koshelok.types.OperationType;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

@Service
@RequiredArgsConstructor
public class OperationService {

    private final OperationConverter converter;

    private final OperationRepository repository;

    private final WalletRepository walletRepository;

    private final CategoryRepository categoryRepository;

    private final PersonRepository personRepository;

    private final CurrencyLogRepository currencyLogRepository;


    @Transactional
    public OperationResponseDto createOperation(Long walletId, OperationRequestDto dto, String email) {
        personRepository.findByEmail(email).orElseThrow(
                () -> new AuthenticationException(email)
        );

        var operation = converter.convert(dto);
        var wallet = walletRepository.findById(walletId)
                .orElseThrow(() -> new NotFoundException(Wallet.class, walletId));
        var category = categoryRepository.findByIdAndType(dto.getCategoryId(), dto.getType())
                .orElseThrow(() -> new NotFoundException(Category.class, dto.getCategoryId()));
        var person = personRepository.findById(wallet.getPerson().getId())
                .orElseThrow(() -> new NotFoundException(Person.class, wallet.getPerson().getId()));

        operation.setWallet(wallet);
        operation.setCategory(category);

        BigDecimal nominal = operation.getBalance();
        if (wallet.getCurrency() != CurrencyType.RUB) {
            nominal = convertCurrency(nominal, operation.getDate().toLocalDate(), wallet.getCurrency());
        }
        if (operation.getType() == OperationType.INCOME) {
            wallet.setIncome(wallet.getIncome().add(operation.getBalance()));
            wallet.setBalance(wallet.getBalance().add(operation.getBalance()));

            person.setIncome(person.getIncome().add(nominal));
            person.setBalance(person.getBalance().add(nominal));
        } else {
            wallet.setSpendings(wallet.getSpendings().add(operation.getBalance()));
            wallet.setBalance(wallet.getBalance().subtract(operation.getBalance()));

            person.setSpendings(person.getSpendings().add(nominal));
            person.setBalance(person.getBalance().subtract(nominal));
        }
        walletRepository.save(wallet);
        personRepository.save(person);
        return converter.convert(repository.save(operation));
    }

    @Transactional
    public void deleteOperationById(Long id, Long walletId, String email) {
        personRepository.findByEmail(email).orElseThrow(
                () -> new AuthenticationException(email)
        );

        var operation = getOperationById(id, email);
        var wallet = walletRepository.findById(walletId)
                .orElseThrow(() -> new NotFoundException(Wallet.class, walletId));
        var person = personRepository.findById(wallet.getPerson().getId())
                .orElseThrow(() -> new NotFoundException(Person.class, wallet.getPerson().getId()));

        BigDecimal nominal = operation.getBalance();
        if (wallet.getCurrency() != CurrencyType.RUB) {
            nominal = convertCurrency(nominal, operation.getDate().toLocalDate(), wallet.getCurrency());
        }
        if (operation.getType() == OperationType.INCOME) {
            wallet.setIncome(wallet.getIncome().subtract(operation.getBalance()));
            wallet.setBalance(wallet.getBalance().subtract(operation.getBalance()));

            person.setIncome(person.getIncome().subtract(nominal));
            person.setBalance(person.getBalance().subtract(nominal));
        } else {
            wallet.setSpendings(wallet.getSpendings().subtract(operation.getBalance()));
            wallet.setBalance(wallet.getBalance().add(operation.getBalance()));

            person.setSpendings(person.getSpendings().subtract(nominal));
            person.setBalance(person.getBalance().add(nominal));
        }
        walletRepository.save(wallet);
        personRepository.save(person);

        repository.deleteById(id);
    }

    @Transactional
    public void deleteAllWalletOperations(Long walletId, String email) {
        personRepository.findByEmail(email).orElseThrow(
                () -> new AuthenticationException(email)
        );

        walletRepository.findById(walletId).orElseThrow(
                () -> new NotFoundException(Wallet.class, walletId)
        );
        var operations = repository.findAllByWalletId(walletId);
        for (Operation operation : operations) {
            deleteOperationById(operation.getId(), walletId, email);
        }
    }

    @Transactional(readOnly = true)
    public List<OperationResponseDto> getAllOperationsByWalletId(Long walletId, String email) {
        personRepository.findByEmail(email).orElseThrow(
                () -> new AuthenticationException(email)
        );

        var operations = repository.findAllByWalletId(walletId);
        // TODO: лист может быть пустым, будет ли работать?
        return operations.stream().map(converter::convert).toList();
    }

    @Transactional(readOnly = true)
    public OperationResponseDto getOperationById(Long id, String email) {
        personRepository.findByEmail(email).orElseThrow(
                () -> new AuthenticationException(email)
        );

        var operation = repository.findById(id).orElseThrow(
                () -> new NotFoundException(Operation.class, id)
        );
        return converter.convert(operation);
    }

    private BigDecimal convertCurrency(BigDecimal nominal, LocalDate date, CurrencyType type) {
        // TODO если за эту дату нет курса валют в бд (потому что выбранная дата раньше, чем дата создания приложения),
        // TODO подтягивается самая ранняя запись в бд
        var currencyLog = currencyLogRepository.findFirstByDateGreaterThanEqual(date)
                .orElseThrow(() -> new NotFoundException(CurrencyLog.class, -1L));
        switch (type.name()) {
            case "USD" -> nominal = nominal.multiply(currencyLog.getUsd());
            case "EUR" -> nominal = nominal.multiply(currencyLog.getEur());
            case "CHF" -> nominal = nominal.multiply(currencyLog.getChf());
            case "GBP" -> nominal = nominal.multiply(currencyLog.getGbp());
            case "JPY" -> nominal = nominal.multiply(currencyLog.getJpy());
            case "SEK" -> nominal = nominal.multiply(currencyLog.getSek());
        }
        return nominal;
    }
}
