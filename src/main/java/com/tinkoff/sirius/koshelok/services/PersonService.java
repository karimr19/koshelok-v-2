package com.tinkoff.sirius.koshelok.services;

import com.tinkoff.sirius.koshelok.converters.PersonConverter;
import com.tinkoff.sirius.koshelok.dto.PersonDto;
import com.tinkoff.sirius.koshelok.exceptions.AuthenticationException;
import com.tinkoff.sirius.koshelok.models.Wallet;
import com.tinkoff.sirius.koshelok.repositories.PersonRepository;
import com.tinkoff.sirius.koshelok.repositories.WalletRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;

@Service
@RequiredArgsConstructor
public class PersonService {
    private final PersonRepository personRepository;

    private final WalletRepository walletRepository;

    private final WalletService walletService;

    private final PersonConverter converter;

    @Transactional
    public PersonDto createPerson(PersonDto personDto) {
        var checkPerson = personRepository.findByEmail(personDto.getEmail());
        if (checkPerson.isPresent()) {
            return converter.convert(checkPerson.get());
        }
        var person = converter.convert(personDto);
        person.setIncome(new BigDecimal(0));
        person.setSpendings(new BigDecimal(0));
        person.setBalance(new BigDecimal(0));
        return converter.convert(personRepository.save(person));
    }

    @Transactional
    public void deletePersonByEmail(String email) {
        var person = personRepository.findByEmail(email).orElseThrow(
                () -> new AuthenticationException(email)
        );
        var wallets = walletRepository.findAllByPersonEmail(email);
        for (Wallet wallet : wallets) {
            walletService.deleteWallet(wallet.getId(), email);
        }
        personRepository.deleteById(person.getId());
    }

    public PersonDto getPersonByEmail(String email) {
        var person = personRepository.findByEmail(email).orElseThrow(
                () -> new AuthenticationException(email)
        );
        return converter.convert(person);
    }

}
