package com.tinkoff.sirius.koshelok.repositories;

import com.tinkoff.sirius.koshelok.models.CurrencyLog;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

public interface CurrencyLogRepository extends JpaRepository<CurrencyLog, Long> {
    Optional<CurrencyLog> findByDate(LocalDate date);

    List<CurrencyLog> findFirst2ByOrderByIdDesc();

    Optional<CurrencyLog> findFirstByDateGreaterThanEqual(LocalDate date);
}
