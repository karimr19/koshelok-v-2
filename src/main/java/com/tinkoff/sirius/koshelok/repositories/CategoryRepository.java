package com.tinkoff.sirius.koshelok.repositories;

import com.tinkoff.sirius.koshelok.models.Category;
import com.tinkoff.sirius.koshelok.types.OperationType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface CategoryRepository extends JpaRepository<Category, Long> {
    @Query("select c from Category c where (c.person.id = ?1 or c.person.id = -1) and c.type = ?2")
    List<Category> findCategoriesByPersonIdAndType(Long personId, OperationType categoryType);

    Optional<Category> findByIdAndType(Long id, OperationType type);
}
