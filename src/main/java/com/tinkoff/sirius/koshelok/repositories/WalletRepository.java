package com.tinkoff.sirius.koshelok.repositories;

import com.tinkoff.sirius.koshelok.models.Wallet;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface WalletRepository extends JpaRepository<Wallet, Long> {
    List<Wallet> findAllByPersonId(Long personId);

    Optional<Wallet> findByIdAndPersonId(Long id, Long personId);
    void deleteByIdAndPersonId(Long id, Long personId);

    boolean existsWalletByIdAndPersonId(Long id, Long personId);
    boolean existsWalletById(Long id);

    List<Wallet> findAllByPersonEmail(String email);

}
